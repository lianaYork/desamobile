import React from 'react'
import { YellowBox } from 'react-native'
import { Provider } from 'react-redux'
import {
  createStore,
  applyMiddleware,
  compose
} from 'redux'
import { StyleProvider, Root } from 'native-base'
import thunkMiddleware from 'redux-thunk'
import config from 'utils/config'
import getTheme from './native-base-theme/components'
import material from './native-base-theme/variables/material'
import Routes from './src/Routes'
import reducers from './src/reducers'

const store = createStore(
  reducers,
  compose(
    applyMiddleware(
      thunkMiddleware
    )
  )
)

export default class App extends React.Component {
  render () {
    global.XMLHttpRequest = global.originalXMLHttpRequest
      ? global.originalXMLHttpRequest
      : global.XMLHttpRequest
    global.FormData = global.originalFormData
      ? global.originalFormData
      : global.FormData

    fetch // Ensure to get the lazy property

    if (window.__FETCH_SUPPORT__) {
      // it's RNDebugger only to have
      window.__FETCH_SUPPORT__.blob = false
    } else {
      /*
       * Set __FETCH_SUPPORT__ to false is just work for `fetch`.
       * If you're using another way you can just use the native Blob and remove the `else` statement
       */
      global.Blob = global.originalBlob ? global.originalBlob : global.Blob
      global.FileReader = global.originalFileReader
        ? global.originalFileReader
        : global.FileReader
    }
    YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader'])
    console.ignoredYellowBox = ['Remote debugger']

    return (
      <Provider
        style={{
          fontFamily: 'Lato, Roboto, "Helvetica Neue", Helvetica, Arial, "\\6587泉驛正黑", "WenQuanYi Zen Hei", "Hiragino Sans GB", "\\5137黑Pro", "LiHei Pro", "Heiti TC", "\\5FAE軟正黑體", "Microsoft JhengHei UI", "Microsoft JhengHei", sans-serif'
        }}
        store={store}
      >
        <Root>
          <StyleProvider style={getTheme(material)}>
            <Routes />
          </StyleProvider>
        </Root>
      </Provider>
    )
  }
}
