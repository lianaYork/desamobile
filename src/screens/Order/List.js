import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet
} from 'react-native'
import {
  List,
  ListItem,
  Text,
  View
} from 'native-base'
import FastImage from 'react-native-fast-image'
import color from 'theme/color'
import globalStyle from 'theme/style'
import { IMAGEURL } from 'utils/config'
import NumberCurrency from 'components/NumberCurrency'

const BORDER_RADIUS = 10
const styles = StyleSheet.create({
  item: {
    width: '48%',
    margin: '1%'
  },
  cardItem: {
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
    alignItems: 'flex-start',
    paddingVertical: 0,
    padding: 0,
    borderRadius: BORDER_RADIUS,
    marginLeft: 0,
    borderBottomWidth: 1
  },
  icon: {
    height: 90,
    width: 130
  },
  rightItem: {
    flex: 1,
    flexDirection: 'column'
  },
  rightItemTop: {
    flex: 1,
    paddingHorizontal: 5
  },
  leftItemRow: {
    flexDirection: 'row'
  },
  rightItemBottom: {
    padding: 5,
    paddingLeft: 10,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  leftItem: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  leftItemTop: {
    flex: 1,
    flexDirection: 'row',
    padding: 5
  },
  leftItemBottom: {
    padding: 5,
    paddingLeft: 10,
    flexDirection: 'column'
  }
})

class ListData extends PureComponent {
  render () {
    const { data, navigation } = this.props
    return (
      <List>
        {data.map((item, index) => (
          <ListItem
            onPress={() => navigation.navigate('DetailOrder', {
              plans: item.plans,
              product: {
                ...item.product,
                paidStatus: item.paidStatus,
                participants: item.participants,
                order_id: item.id,
                city: item.city,
                province: item.province
              }
            })}
            key={index}
            style={styles.cardItem}
          >
            <FastImage
              style={styles.icon}
              source={{
                uri: `${IMAGEURL}${item.product.image[0].url}`
              }}
              resizeMode={FastImage.resizeMode.cover}
            />
            <View style={styles.rightItem}>
              <View style={styles.rightItemTop}>
                <Text style={globalStyle.h4}>{item.product.name}</Text>
                <Text style={[globalStyle.text, { fontSize: 12 }]}>{`Status: ${item.orderStatus}`}</Text>
                <Text style={[globalStyle.text, { fontSize: 12 }]}>{`Partisipan: ${item.participants.length}`}</Text>
                {<Text style={[globalStyle.h4, { color: color.primaryColor }]}>{item.paidStatus ? item.orderStatus : 'Waiting'}</Text>}
              </View>
            </View>

            <View style={styles.leftItem}>
              <View style={styles.leftItemTop} />
              <View style={styles.leftItemBottom}>
                {!item.product.status && (
                  <Text
                    style={[
                      globalStyle.h4,
                      {
                        color: item.product.status ? color.secondaryText : color.errorColor,
                        textAlign: 'right'
                      }]}
                  >
                    Habis
                  </Text>
                )}
                <View>
                  <NumberCurrency
                    value={item.price}
                    style={globalStyle.text}
                  />
                </View>
              </View>
            </View>
          </ListItem>
        ))}
      </List>
    )
  }
}

ListData.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.array
}

ListData.defaultProps = {
  data: []
}

export default ListData
