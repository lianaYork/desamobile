import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StatusBar,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from 'react-native'
import {
  View,
  Text,
  Container
} from 'native-base'
import color from 'theme/color'
import { getData } from 'actions/orderAction'
import List from './List'

class Order extends PureComponent {
  state = {
    refreshing: false
  }

  componentWillMount () {
    this._refresh()
  }

  _refresh = () => {
    const { getData } = this.props
    getData()
  }

  render () {
    const { listOrder, loadingOrder, navigation } = this.props
    const { refreshing } = this.state

    return (
      <Container>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this._refresh()}
            />
          )}
        >
          {loadingOrder && <ActivityIndicator style={{ marginVertical: 10 }} />}
          {listOrder.length > 0 ? (
            <List data={listOrder} navigation={navigation} />
          )
            : (
              <View
                style={{
                  marginTop: '50%'
                }}
              >
                <Text style={{ justifyContent: 'center', textAlign: 'center', color: color.secondaryText }}>Belum ada pemesanan, ayo mulai sekarang</Text>
              </View>
            )}
        </ScrollView>
      </Container>
    )
  }
}

Order.propTypes = {
  navigation: PropTypes.object.isRequired,
  listOrder: PropTypes.array
}

Order.defaultProps = {
  listOrder: []
}

const mapStateToProps = state => ({
  loadingOrder: state.order.loadingOrder,
  listOrder: state.order.listOrder
})

const mapDispatchToProps = dispatch => ({
  getData: () => dispatch(getData())
})

export default connect(mapStateToProps, mapDispatchToProps)(Order)
