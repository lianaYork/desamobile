import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native'
import {
  View
} from 'native-base'
import MediaItem from 'components/MediaItem'
import color from 'theme/color'

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 10
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  item: {
    width: '48%',
    margin: '1%'
  },
  button: {
    borderWidth: 1 / 2,
    borderColor: color.secondaryText,
    borderRadius: 7
  }
})

class Product extends PureComponent {
  render () {
    const { navigation, listPopuler } = this.props
    return (
      <View style={styles.content}>
        <View style={styles.container}>
          {listPopuler.map((data, index) => (
            <TouchableWithoutFeedback
              key={index}
              onPress={() => navigation.navigate('DetailProduct', {
                product: data
              })}
            >
              <View
                style={styles.item}
              >
                <MediaItem
                  data={data}
                />
              </View>
            </TouchableWithoutFeedback>
          ))}
        </View>
      </View>
    )
  }
}

Product.propTypes = {
  navigation: PropTypes.object.isRequired,
  listPopuler: PropTypes.array
}

Product.defaultProps = {
  listPopuler: []
}

const mapStateToProps = state => ({
  listPopuler: state.product.listPopuler
})

export default connect(mapStateToProps)(Product)
