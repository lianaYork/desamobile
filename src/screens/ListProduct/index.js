import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  StatusBar,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  Container
} from 'native-base'
import color from 'theme/color'
import SearchBar from 'components/SearchBar'
import Product from './Product'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  }
})

class ListProduct extends PureComponent {
  state = {
    q: '',
    populer: false,
    refreshing: false
  }

  componentWillMount () {
    const { navigation } = this.props
    const { q, populer } = navigation.state.params
    if (q) {
      this.setState({ q })
    }
    if (populer) {
      this.setState({ populer })
    }
  }

  _refresh = () => {
    console.log('refresh')
  }

  render () {
    const { navigation } = this.props
    const { q, populer, refreshing } = this.state
    console.log('populer', populer)

    return (
      <Container style={styles.container}>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this._refresh()}
            />
          )}
        >
          <SearchBar back defaultValue={q} navigation={navigation} />
          <Product navigation={navigation} />
        </ScrollView>
      </Container>
    )
  }
}

ListProduct.propTypes = {
  navigation: PropTypes.object.isRequired
}

export default ListProduct
