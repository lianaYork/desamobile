import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet
} from 'react-native'
import {
  Text,
  View,
  Icon,
  Left,
  Right
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'
import NumberCurrency from 'components/NumberCurrency'

const styles = StyleSheet.create({
  content: {
    padding: 10,
    marginBottom: 10
  },
  location: {
    flexDirection: 'row'
  }
})

class ProductTitle extends PureComponent {
  render () {
    const { product } = this.props
    return (
      <View style={styles.content}>
        <View style={styles.location}>
          <Icon
            name="location-on"
            type="MaterialIcons"
            style={[
              globalStyle.h5,
              {
                width: 25,
                color: color.secondaryText,
                fontSize: 20
              }
            ]}
          />
          <Text style={globalStyle.h5}>{`${product.city.name} - ${product.province.name}`}</Text>
        </View>
        <Text style={globalStyle.h2}>{`${product.name}`}</Text>
        <View style={styles.location}>
          <Left>
            <NumberCurrency
              value={product.price}
              style={globalStyle.text}
            />
          </Left>
          <Right>
            <NumberCurrency
              value={product.priceDiscount}
              style={[
                globalStyle.text,
                {
                  fontSize: 12,
                  color: color.errorColor,
                  textDecorationLine: 'line-through',
                  textDecorationStyle: 'solid',
                  textAlign: 'right'
                }]}
            />
          </Right>
        </View>
      </View>
    )
  }
}

ProductTitle.propTypes = {
  product: PropTypes.object
}

ProductTitle.defaultProps = {
  product: {}
}

export default ProductTitle
