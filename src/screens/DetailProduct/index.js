import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  TouchableOpacity,
  StatusBar,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  View,
  Container,
  Icon,
  DatePicker,
  Button,
  Text
} from 'native-base'
import moment from 'moment'
import globalStyle from 'theme/style'
import color from 'theme/color'
import { changeDate, resetDate } from 'actions/orderAction'
import Header from './Header'
import Segment from './Segment'
import ProductTitle from './ProductTitle'
import ListDescription from './ListDescription'

class DetailProduct extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = 'Detail Produk'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="share-google"
              type="EvilIcons"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  state = {
    product: null,
    refreshing: false
  }

  componentWillMount () {
    this.getProduct()
  }

  componentWillUnmount () {
    const { resetDate } = this.props
    resetDate()
  }

  getProduct () {
    const { navigation } = this.props
    if (navigation.state.params) {
      const { product } = navigation.state.params
      this.setProduct(product)
    }
  }

  setProduct (product) {
    this.setState({ product })
  }

  _refresh = () => {
    console.log('refresh')
  }

  setDate = (newDate) => {
    const { changeDate, navigation } = this.props
    const { product } = this.state
    changeDate({ date: newDate, navigation, product })
  }

  render () {
    const { navigation, orderDate } = this.props
    const { product, refreshing } = this.state

    return (
      <Container>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this._refresh()}
            />
          )}
        >
          <Header navigation={navigation} />
          <Segment product={product} />
          <ProductTitle product={product} />
          <ListDescription product={product} />
          <View style={{ padding: 10 }}>
            {orderDate ? (
              <Button style={[globalStyle.buttonCenter, { backgroundColor: color.primaryColor }]}>
                <Text>{moment(orderDate).format('DD MMM YYYY')}</Text>
              </Button>
            )
              : (
                <DatePicker
                  minimumDate={new Date(2019, 4, 2)}
                  locale="id  "
                  timeZoneOffsetInMinutes={undefined}
                  modalTransparent={false}
                  animationType="fade"
                  androidMode="default"
                  placeHolderText="PEMESANAN"
                  textStyle={{
                    color: color.textIcons,
                    width: '100%',
                    textAlign: 'center',
                    backgroundColor: color.primaryColor,
                    borderRadius: 5
                  }}
                  placeHolderTextStyle={{
                    color: color.textIcons,
                    width: '100%',
                    textAlign: 'center',
                    backgroundColor: color.primaryColor,
                    borderRadius: 5
                  }}
                  onDateChange={date => this.setDate(date)}
                  disabled={false}
                />
              )}
          </View>
        </ScrollView>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  orderDate: state.order.orderDate
})

const mapDispatchToProps = dispatch => ({
  changeDate: date => dispatch(changeDate(date)),
  resetDate: () => dispatch(resetDate())
})

export default connect(mapStateToProps, mapDispatchToProps)(DetailProduct)
