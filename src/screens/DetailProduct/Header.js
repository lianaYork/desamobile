import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StyleSheet
} from 'react-native'
import {
  Icon,
  Header,
  Item,
  Right
} from 'native-base'
import color from 'theme/color'

const styles = StyleSheet.create({
  header: {
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  item: {
    borderBottomWidth: 0,
    marginLeft: 0
  }
})

class DetailProduct extends PureComponent {
  render () {
    const { navigation } = this.props
    return (
      <Header style={styles.header}>
        <Item onPress={this.handleGoToSearch} style={styles.item}>
          <TouchableOpacity onPress={() => navigation.goBack(null)}>
            <Icon
              name="chevron-left"
              type="MaterialCommunityIcons"
              style={{
                fontSize: 40,
                color: color.textIcons
              }}
            />
          </TouchableOpacity>
        </Item>
        <Right>
          <Icon
            name="share"
            type="MaterialIcons"
            style={{
              fontSize: 30,
              color: color.textIcons
            }}
          />
        </Right>
      </Header>
    )
  }
}

export default DetailProduct
