const validate = ({
  firstName,
  lastName,
  birthDate,
  idNo,
  order
}) => {
  const errors = {
    isAccount: '',
    firstName: '',
    lastName: '',
    birthDate: '',
    gender: '',
    idNo: '',
    order: ''
  }

  errors.firstName = !firstName ? '*' : undefined
  errors.lastName = !lastName ? '*' : undefined
  errors.birthDate = !birthDate ? '*' : undefined
  errors.idNo = !idNo ? '*' : undefined
  errors.order = !order ? '*' : undefined

  return errors
}

export default validate
