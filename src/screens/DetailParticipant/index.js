import React, { PureComponent } from 'react'
import {
  StatusBar,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  Container,
  Toast
} from 'native-base'
import color from 'theme/color'
import { getById } from 'services/participantService'
import Header from 'components/Header'
import DetailParticipantForm from './DetailParticipantForm'

class Participant extends PureComponent {
  state = {
    type: 'add',

    participant: {},
    refreshing: false
  }

  componentWillMount () {
    this.setParticipant()
  }

  componentWillUnmount () {
    this.setState({ participant: [] })
  }

  setParticipant = async () => {
    const { navigation } = this.props
    const { type, id } = navigation.state.params
    if (type === 'edit') {
      try {
        const orderData = await getById(id)
        if (orderData.success) {
          this.setState({ participant: orderData.data })
        } else {
          Toast.show({
            text: orderData.message,
            buttonText: 'Okay',
            type: 'warning'
          })
        }
      } catch (error) {
        Toast.show({
          text: 'Error retrieve Data',
          buttonText: 'Okay',
          type: 'warning'
        })
      }
    }

    this.setState({ type })
  }

  render () {
    const { navigation } = this.props
    const { participant, type, refreshing } = this.state

    return (
      <Container>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this.setParticipant()}
            />
          )}
        >
          <Header goBack={() => navigation.goBack(null)} name={`${type === 'add' ? 'Tambah' : 'Ubah'} Partisipan`} />
          <DetailParticipantForm participant={participant} navigation={navigation} />
        </ScrollView>
      </Container>
    )
  }
}

export default Participant
