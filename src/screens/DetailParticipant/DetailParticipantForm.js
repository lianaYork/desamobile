import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import {
  StyleSheet,
  KeyboardAvoidingView
} from 'react-native'
import {
  Input,
  Item,
  View,
  Text,
  Button,
  Toast,
  CheckBox,
  Body,
  ListItem,
  Radio,
  DatePicker
} from 'native-base'
import moment from 'moment'
import color from 'theme/color'
import { set as setParticipant } from 'services/participantService'
import validate from './validate'

const styles = StyleSheet.create({
  container: {
    padding: 10
  },
  label: {
    color: color.primaryText,
    fontSize: 14,
    marginBottom: 5
  },
  viewInput: {
    marginBottom: 10
  },
  input: {
    paddingLeft: 10,
    paddingRight: 10,
    color: color.primaryText,
    borderWidth: 1,
    fontSize: 14,
    borderColor: color.dividerColor,
    borderRadius: 5,
    height: 40
  },
  item: {
    borderBottomWidth: 0
  },
  hairline: {
    backgroundColor: color.secondaryText,
    height: 1,
    width: '40%'
  },
  text: {
    alignSelf: 'center',
    color: color.primaryText
  }
})

class DetailParticipantForm extends PureComponent {
  state = {
    isAccount: false,
    gender: 'male',
    birthDate: null
  }

  onSubmit = async (values) => {
    const { navigation, error } = this.props
    const { order, refresh } = navigation.state.params
    const {
      isAccount,
      gender,
      birthDate
    } = this.state
    if (!error) {
      try {
        const participantData = await setParticipant({
          isAccount,
          firstName: values.firstName,
          lastName: values.lastName,
          birthDate: moment(birthDate).format('YYYY-MM-DD'),
          gender,
          idNo: values.idNo,
          order
        })
        if (participantData.success) {
          Toast.show({
            text: 'Partisipan berhasil ditambah',
            buttonText: 'Ok',
            type: 'success'
          })
          refresh()
          navigation.navigate('Participant', {
            order
          })
        } else {
          this.errorHandler(participantData.message)
        }
      } catch (error) {
        this.errorHandler(error.message)
      }
    }
  }

  errorHandler = (message) => {
    Toast.show({
      text: JSON.stringify(message),
      buttonText: 'Ok',
      type: 'warning'
    })
  }

  changeValue = async (text, key) => {
    const { change } = this.props
    await change(key, text)
  }

  inputComponent = ({
    input, type = 'default', placeholder = '', meta: { error, touched }
  }) => {
    return (
      <Item
        style={styles.item}
        error={!!error && touched}
      >
        <Input
          selectTextOnFocus
          style={styles.input}
          placeholder={placeholder}
          keyboardType={type}
          {...input}
        />
        {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
      </Item>
    )
  }

  setMyProfile = async () => {
    this.setState(state => ({ isAccount: !state.isAccount }))
  }

  render () {
    const {
      handleSubmit,
      submitting,
      invalid,
      error,
      navigation
    } = this.props

    const {
      isAccount,
      gender
    } = this.state

    return (
      <View>
        <KeyboardAvoidingView
          style={styles.container}
          behavior="padding"
        >
          <View style={styles.viewInput}>
            <Text style={styles.label}>Data Saya</Text>
            <ListItem
              style={{
                borderBottomWidth: 0,
                paddingTop: 0,
                paddingBottom: 0
              }}
            >
              <CheckBox
                onPress={() => this.setMyProfile()}
                color={color.primaryColor}
                checked={isAccount}
              />
              <Body>
                <Text style={styles.label}>Gunakan data saya</Text>
              </Body>
            </ListItem>
          </View>
          <View style={styles.viewInput}>
            <Text style={styles.label}>Nama Awal</Text>
            <Field
              name="firstName"
              type="default"
              placeholder="e.g. John"
              component={this.inputComponent}
            />
          </View>
          <View style={styles.viewInput}>
            <Text style={styles.label}>Nama Akhir</Text>
            <Field
              name="lastName"
              type="default"
              placeholder="e.g. Doe"
              component={this.inputComponent}
            />
          </View>
          <View style={styles.viewInput}>
            <Text style={styles.label}>Tanggal Lahir</Text>
            <DatePicker
              locale="id"
              timeZoneOffsetInMinutes={undefined}
              modalTransparent={false}
              animationType="fade"
              androidMode="default"
              placeHolderText="DD/MM/YYYY"
              textStyle={{
                color: color.secondaryText,
                width: '100%',
                borderRadius: 5
              }}
              placeHolderTextStyle={{
                color: color.secondaryText,
                borderRadius: 5
              }}
              onDateChange={date => this.setState({ birthDate: date })}
              disabled={false}
            />
          </View>
          <View style={styles.viewInput}>
            <Text style={styles.label}>Jenis Kelamin</Text>
            <Item style={{ borderBottomWidth: 0 }}>
              <Radio onPress={() => this.setState({ gender: 'male' })} selected={gender === 'male'} />
              <Text> Laki-laki</Text>
            </Item>
            <Item style={{ borderBottomWidth: 0 }}>
              <Radio onPress={() => this.setState({ gender: 'female' })} selected={gender === 'female'} />
              <Text> Perempuan</Text>
            </Item>
          </View>

          <View style={styles.viewInput}>
            <Text style={styles.label}>No KTP</Text>
            <Field
              name="idNo"
              type="default"
              placeholder="e.g. 127109210291301"
              component={this.inputComponent}
            />
          </View>

          <View
            style={styles.viewInput}
          >
            <Button
              primary
              block
              disabled={invalid || submitting}
              onPress={handleSubmit(values => this.onSubmit(values, navigation))}
            >
              <Text
                style={[
                  styles.text,
                  { color: color.textIcons }
                ]}
              >
                Tambahkan
              </Text>
            </Button>
          </View>
          {error && <Text>{error}</Text>}
        </KeyboardAvoidingView>
      </View>
    )
  }
}

export default reduxForm({
  form: 'DetailParticipantForm',
  validate
})(DetailParticipantForm)
