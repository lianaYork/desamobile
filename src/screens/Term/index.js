import React, { PureComponent } from 'react'
import {
  StatusBar
} from 'react-native'
import {
  Content
} from 'native-base'
import color from 'theme/color'
import Header from 'components/Header'

class Term extends PureComponent {
  render () {
    const { navigation } = this.props
    return (
      <Content>
        <StatusBar backgroundColor={color.primaryColor} />
        <Header goBack={() => navigation.goBack(null)} name="Syarat dan ketentuan" />
      </Content>
    )
  }
}

export default Term
