import React, { PureComponent } from 'react'
import {
  StatusBar,
  ScrollView,
  RefreshControl,
  StyleSheet
} from 'react-native'
import {
  Container,
  Text,
  Toast,
  ListItem,
  View,
  Button
} from 'native-base'
import color from 'theme/color'
import { get } from 'services/participantService'
import Header from 'components/Header'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  listItem: {
    borderTopWidth: 0,
    borderBottomWidth: 1,
    marginLeft: 0,
    paddingHorizontal: 10,
    backgroundColor: color.textIcons
  }
})

class Participant extends PureComponent {
  state = {
    participant: [],
    refreshing: false
  }

  componentWillMount () {
    this.setOrder()
  }

  componentWillUnmount () {
    this.setState({ participant: [] })
  }

  setOrder = async () => {
    const { navigation } = this.props
    const { order } = navigation.state.params
    try {
      const orderData = await get({
        order
      })
      if (orderData.success) {
        this.setState({ participant: orderData.data })
      } else {
        Toast.show({
          text: orderData.message,
          buttonText: 'Okay',
          type: 'warning'
        })
      }
    } catch (error) {
      Toast.show({
        text: 'Error retrieve Data',
        buttonText: 'Okay',
        type: 'warning'
      })
    }
  }

  render () {
    const { navigation } = this.props
    const { order, paidStatus } = navigation.state.params
    const { participant, refreshing } = this.state
    return (
      <Container>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          style={{ marginBottom: 70 }}
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this.setOrder()}
            />
          )}
        >
          <Header goBack={() => navigation.goBack(null)} name="Partisipan" />
          {participant.length === 0 && (
            <Text style={[globalStyle.h4, { textAlign: 'center', padding: 40 }]}>Belum ada partisipan</Text>
          )}
          {participant.map((data, index) => (
            <ListItem
              key={index}
              onPress={() => console.log('partisipan')}
              style={styles.listItem}
            >
              <Text style={[globalStyle.h2, { fontWeight: '300' }]}>{`Partisipan ${index + 1} (${(data.firstName || ''.trim())} ${(data.lastName || '').trim()})`}</Text>
            </ListItem>
          ))}
        </ScrollView>
        {!paidStatus && (
          <View
            style={{
              bottom: 0, left: 0, right: 0, position: 'absolute', padding: 10
            }}
          >
            <Button
              onPress={() => navigation.navigate('DetailParticipant', { type: 'add', order, refresh: () => this.setOrder() })}
              style={[globalStyle.buttonCenter, { backgroundColor: color.primaryColor }]}
            >
              <Text>Tambah Partisipan</Text>
            </Button>
          </View>
        )}
      </Container>
    )
  }
}

export default Participant
