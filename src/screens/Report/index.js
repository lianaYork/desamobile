import React, { PureComponent } from 'react'
import {
  StatusBar,
  Platform,
  BackHandler,
  Alert
} from 'react-native'
import {
  Content
} from 'native-base'
import color from 'theme/color'
import Header from 'components/Header'

class Report extends PureComponent {
  componentDidMount () {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.goBack)
    }
  }

  componentWillUnmount () {
    if (Platform.OS === 'android') {
      BackHandler.removeEventListener('hardwareBackPress', this.goBack)
    }
  }

  goBack = () => {
    const { navigation } = this.props
    Alert.alert(
      'Kembali ?',
      '',
      [
        {
          text: 'Batalkan',
          onPress: () => { },
          style: 'cancel'
        },
        { text: 'OK', onPress: () => navigation.goBack(null) }
      ],
      { cancelable: false },
    )
    return true
  }

  render () {
    return (
      <Content>
        <StatusBar backgroundColor={color.primaryColor} />
        <Header goBack={() => this.goBack()} name="Lapor dan hubungi" />
      </Content>
    )
  }
}

export default Report
