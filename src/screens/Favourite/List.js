import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet
} from 'react-native'
import {
  List,
  ListItem,
  Content,
  Icon,
  Text,
  View
} from 'native-base'
import FastImage from 'react-native-fast-image'
import color from 'theme/color'
import globalStyle from 'theme/style'
import { IMAGEURL } from 'utils/config'

const BORDER_RADIUS = 10
const styles = StyleSheet.create({
  item: {
    width: '48%',
    margin: '1%'
  },
  cardItem: {
    paddingRight: 10,
    paddingTop: 0,
    paddingBottom: 0,
    alignItems: 'flex-start',
    paddingVertical: 0,
    padding: 0,
    borderRadius: BORDER_RADIUS,
    marginLeft: 0,
    borderBottomWidth: 1
  },
  icon: {
    height: 90,
    width: 130
  },
  rightItem: {
    flex: 1,
    flexDirection: 'column'
  },
  rightItemTop: {
    flex: 1,
    flexDirection: 'row',
    padding: 5
  },
  rightItemBottom: {
    padding: 5,
    paddingLeft: 10,
    flexDirection: 'column',
    alignItems: 'flex-start'
  },
  leftItem: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  leftItemTop: {
    flex: 1,
    flexDirection: 'row',
    padding: 5
  },
  leftItemBottom: {
    padding: 5,
    paddingLeft: 10,
    flexDirection: 'column'
  }
})

class ListData extends PureComponent {
  render () {
    const { data, navigation } = this.props
    return (
      <Content>
        <List>
          {data.map((item, index) => (
            <ListItem
              onPress={() => {
                if (item.status) {
                  navigation.navigate('DetailProduct', {
                    product: item
                  })
                }
              }}
              key={index}
              style={styles.cardItem}
            >
              <FastImage
                style={styles.icon}
                source={{
                  uri: `${IMAGEURL}${item.image.url}`
                }}
                resizeMode={FastImage.resizeMode.cover}
              />
              <View style={styles.rightItem}>
                <View style={styles.rightItemTop}>
                  <Icon
                    name="location-on"
                    type="MaterialIcons"
                    style={{
                      width: 25,
                      color: color.secondaryText,
                      fontSize: 20
                    }}
                  />
                  <Text style={globalStyle.text}>{`${item.city} - `}</Text>
                  <Text style={globalStyle.text}>{item.province}</Text>
                </View>
                <View style={styles.rightItemBottom}>
                  <Text style={globalStyle.h4}>{item.name}</Text>
                  <Text style={globalStyle.text}>{item.priceText}</Text>
                </View>
              </View>

              <View style={styles.leftItem}>
                <View style={styles.leftItemTop}>
                  <Icon
                    name="trash"
                    type="FontAwesome"
                    style={{
                      color: color.secondaryText,
                      fontSize: 24
                    }}
                  />
                </View>
                <View style={styles.leftItemBottom}>
                  {!item.status && (
                    <Text
                      style={[
                        globalStyle.h4,
                        {
                          color: item.status ? color.secondaryText : color.errorColor,
                          textAlign: 'right'
                        }]}
                    >
                      Habis
                    </Text>
                  )}
                  {item.priceDiscountText && (
                    <Text
                      style={[
                        globalStyle.text,
                        {
                          fontSize: 12,
                          color: color.errorColor,
                          textDecorationLine: 'line-through',
                          textDecorationStyle: 'solid',
                          textAlign: 'right'
                        }]}
                    >
                      {item.priceDiscountText}
                    </Text>
                  )}
                </View>
              </View>
            </ListItem>
          ))}
        </List>
      </Content>
    )
  }
}

ListData.propTypes = {
  navigation: PropTypes.object.isRequired,
  data: PropTypes.array
}

ListData.defaultProps = {
  data: []
}

export default ListData
