import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StatusBar,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  Content,
  View,
  Text
} from 'native-base'
import color from 'theme/color'
import List from './List'

class Favourite extends PureComponent {
  state = {
    refreshing: false
  }

  _refresh = () => {
    console.log('refresh')
  }

  render () {
    const { listFavourite, navigation } = this.props
    const { refreshing } = this.state
    return (
      <Content>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this._refresh()}
            />
          )}
        >
          <List data={listFavourite} navigation={navigation} />
          {listFavourite.length > 0 ? (
            <List data={listFavourite} navigation={navigation} />
          )
            : (
              <View
                style={{
                  marginTop: '50%'
                }}
              >
                <Text style={{ justifyContent: 'center', textAlign: 'center', color: color.secondaryText }}>Belum ada yang disukai, ayo mulai sekarang</Text>
              </View>
            )}
        </ScrollView>
      </Content>
    )
  }
}

Favourite.propTypes = {
  listFavourite: PropTypes.array
}

Favourite.defaultProps = {
  listFavourite: []
}

const mapStateToProps = state => ({
  listFavourite: state.favourite.listFavourite
})

export default connect(mapStateToProps)(Favourite)
