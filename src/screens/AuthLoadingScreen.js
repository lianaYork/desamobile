import React from 'react'
import { connect } from 'react-redux'
import {
  ActivityIndicator,
  StatusBar,
  StyleSheet,
  View,
  Image
} from 'react-native'
import { Text } from 'native-base'
import {
  fetchUser
} from 'actions/authAction'
import color from 'theme/color'
import { getUserToken } from 'utils/storage'
import { me } from 'services/userService'
import { APPNAME } from 'utils/config'

const styles = StyleSheet.create({
  container: {
    backgroundColor: color.primaryColor,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center'
  },
  segmentImage: {
    height: 150,
    width: 150,
    backgroundColor: color.primaryColor
  },
  headerText: {
    marginVertical: 10,
    fontSize: 30,
    color: color.textIcons
  }
})

class AuthLoadingScreen extends React.Component {
  constructor (props) {
    super(props)
    this._bootstrapAsync()
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const { navigation } = this.props
    const userToken = await getUserToken()

    if (userToken && userToken !== '') {
      const user = await me()
      if (user.success) {
        navigation.navigate('App')
      } else {
        navigation.navigate('Auth')
      }
    } else {
      navigation.navigate('Auth')
    }
  }

  // Render any loading content that you like here
  render () {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor={color.primaryColor} />
        <Image
          style={styles.segmentImage}
          source={require('assets/app/logo.png')}
          resizeMethod="scale"
        />
        <Text style={styles.headerText}>{APPNAME}</Text>
        <ActivityIndicator />
      </View>
    )
  }
}

const mapStateToProps = state => ({
  isFetching: state.auth.isFetching
})

const mapDispatchToProps = dispatch => ({
  fetchUser: token => dispatch(fetchUser(token))
})

export default connect(mapStateToProps, mapDispatchToProps)(AuthLoadingScreen)
