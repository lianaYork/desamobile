import React, { PureComponent } from 'react'
import {
  StyleSheet,
  StatusBar,
  Alert
} from 'react-native'
import {
  Container,
  Content,
  ListItem,
  Text,
  Separator
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'
import { APPNAME } from 'utils/config'
import { remove } from 'utils/storage'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: color.dividerColor
  },
  content: {
    flex: 1 / 2,
    backgroundColor: color.dividerColor,
    paddingLeft: 0,
    paddingTop: 0,
    paddingBottom: 0
  },
  separator: {
    flex: 1,
    height: 15,
    backgroundColor: color.dividerColor,
    paddingLeft: 0,
    paddingTop: 0,
    paddingBottom: 0
  },
  listItem: {
    borderTopWidth: 1,
    borderBottomWidth: 1,
    marginLeft: 0,
    paddingHorizontal: 10,
    backgroundColor: color.textIcons
  }
})

class Profile extends PureComponent {
  _logout = () => {
    const { navigation } = this.props
    Alert.alert(
      `Keluar dari ${APPNAME}`,
      'Apakah Anda ingin keluar ?',
      [
        {
          text: 'Kembali',
          onPress: () => { },
          style: 'cancel'
        },
        {
          text: 'Iya',
          onPress: async () => {
            console.log('onPress')
            const successRemove = await remove('token')
            console.log('successRemove', successRemove)

            if (successRemove) {
              navigation.navigate('Auth')
            }
          }
        }
      ],
      { cancelable: false },
    )
  }

  render () {
    const { navigation } = this.props
    return (
      <Container style={styles.container}>
        <Content style={styles.content}>
          <StatusBar backgroundColor={color.primaryColor} />
          <ListItem onPress={() => navigation.navigate('PersonalData')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Data Diri</Text>
          </ListItem>
          <ListItem onPress={() => navigation.navigate('History')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Pengalaman</Text>
          </ListItem>

          <Separator bordered style={styles.separator} />
          <ListItem onPress={() => navigation.navigate('Privacy')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Aturan privasi</Text>
          </ListItem>
          <ListItem onPress={() => navigation.navigate('Term')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Syarat dan ketentuan</Text>
          </ListItem>
          <ListItem onPress={() => navigation.navigate('Void')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Pembatalan</Text>
          </ListItem>

          <Separator bordered style={styles.separator} />
          <ListItem onPress={() => console.log('arahkan ke app partnership')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Partnership</Text>
          </ListItem>
          <ListItem onPress={() => navigation.navigate('Report')} style={styles.listItem}>
            <Text style={[globalStyle.h2, { fontWeight: '300' }]}>Lapor dan hubungi</Text>
          </ListItem>

          <Separator bordered style={styles.separator} />
          <ListItem onPress={() => this._logout()} style={styles.listItem}>
            <Text style={[globalStyle.h2, { color: color.errorColor, fontWeight: '300' }]}>Keluar</Text>
          </ListItem>

          <Separator bordered style={styles.separator} />
        </Content>
      </Container>
    )
  }
}

export default Profile
