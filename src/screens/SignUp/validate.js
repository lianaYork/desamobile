const validate = ({
  username,
  email,
  password
}) => {
  const errors = {
    username: '',
    email: '',
    password: ''
  }

  errors.username = !username ? '*' : undefined
  errors.email = !email ? '*' : undefined
  errors.password = !password ? '*' : undefined

  return errors
}

export default validate
