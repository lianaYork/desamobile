import React, { PureComponent } from 'react'
import { StatusBar } from 'react-native'
import {
  View
} from 'native-base'
import color from 'theme/color'
import SignUpForm from './SignUpForm'

class SignUp extends PureComponent {
  render () {
    const { navigation } = this.props
    return (
      <View>
        <StatusBar backgroundColor={color.primaryColor} />
        <SignUpForm navigation={navigation} />
      </View>
    )
  }
}

export default SignUp
