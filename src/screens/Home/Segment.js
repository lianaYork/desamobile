import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet
} from 'react-native'
import {
  View
} from 'native-base'
import FastImage from 'react-native-fast-image'
import { IMAGEURL } from 'utils/config'

const styles = StyleSheet.create({
  content: {
    flex: 1
  },
  segmentImage: {
    height: 150
  }
})

class Segment extends PureComponent {
  render () {
    const { segmentImage } = this.props
    return (
      <View style={styles.content}>
        {segmentImage.image && (
          <FastImage
            style={styles.segmentImage}
            source={{
              uri: `${IMAGEURL}/${segmentImage.image.url}`,
              priority: FastImage.priority.high
            }}
            resizeMode={FastImage.resizeMode.stretch}
          />
        )}
      </View>
    )
  }
}

Segment.propTypes = {
  segmentImage: PropTypes.object
}

Segment.defaultProps = {
  segmentImage: {}
}

const mapStateToProps = state => ({
  segmentImage: state.home.segmentImage
})

export default connect(mapStateToProps)(Segment)
