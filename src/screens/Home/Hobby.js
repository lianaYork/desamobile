import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet
} from 'react-native'
import {
  View,
  Text
} from 'native-base'
import SegmentItem from 'components/SegmentItem'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 10
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

class Hobby extends PureComponent {
  render () {
    const { listHobby, navigation } = this.props
    return (
      <View style={styles.content}>
        {listHobby.length > 0 && <Text style={[globalStyle.h2, { paddingVertical: 5 }]}>Hobi</Text>}
        {listHobby.length > 0 && (
          <View style={styles.container}>
            <SegmentItem type="hobby" data={listHobby} navigation={navigation} />
          </View>
        )}
      </View>
    )
  }
}

Hobby.propTypes = {
  listHobby: PropTypes.array
}

Hobby.defaultProps = {
  listHobby: []
}

const mapStateToProps = state => ({
  listHobby: state.home.listHobby
})

export default connect(mapStateToProps)(Hobby)
