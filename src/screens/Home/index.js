import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet,
  StatusBar,
  ScrollView,
  RefreshControl,
  ActivityIndicator
} from 'react-native'
import {
  Content
} from 'native-base'
import color from 'theme/color'
import SearchBar from 'components/SearchBar'
import { queryHome } from 'actions/homeAction'
import Segment from './Segment'
import Populer from './Populer'
import Hobby from './Hobby'
import Daerah from './Daerah'

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  content: {
    padding: 10
  }
})

class Home extends PureComponent {
  state = {
    refreshing: false
  }

  componentDidMount () {
    this._refresh()
  }

  _refresh = () => {
    const { getHome } = this.props
    getHome()
  }

  render () {
    const { navigation, loadingHome } = this.props
    const { refreshing } = this.state
    return (
      <ScrollView
        scrollEventThrottle={300}
        removeClippedSubviews
        refreshControl={(
          <RefreshControl
            refreshing={refreshing}
            onRefresh={() => this._refresh()}
          />
        )}
      >
        <Content style={styles.container}>
          <StatusBar backgroundColor={color.primaryColor} />
          <Segment />
          <SearchBar navigation={navigation} />
          {loadingHome && <ActivityIndicator style={{ marginVertical: 10 }} />}
          <Populer navigation={navigation} />
          <Hobby navigation={navigation} />
          <Daerah navigation={navigation} />
        </Content>
      </ScrollView>
    )
  }
}

Home.propTypes = {
  navigation: PropTypes.object.isRequired
}

const mapStateToProps = state => ({
  loadingHome: state.home.loadingHome
})

const mapDispatchToProps = dispatch => ({
  getHome: () => dispatch(queryHome())
})

export default connect(mapStateToProps, mapDispatchToProps)(Home)
