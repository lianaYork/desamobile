import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native'
import {
  View,
  Text,
  Button
} from 'native-base'
import MediaItem from 'components/MediaItem'
import color from 'theme/color'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 10
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  item: {
    width: '48%',
    margin: '1%'
  },
  button: {
    borderWidth: 1 / 2,
    borderColor: color.secondaryText,
    borderRadius: 7
  }
})

class Populer extends PureComponent {
  render () {
    const { navigation, listPopuler } = this.props

    return (
      <View style={styles.content}>
        {listPopuler.length > 0 && <Text style={[globalStyle.h2, { padding: 5 }]}>Sedang populer</Text>}
        <View style={styles.container}>
          {listPopuler.map((data, index) => (
            <TouchableWithoutFeedback
              key={index}
              onPress={() => navigation.navigate('DetailProduct', {
                product: data
              })}
            >
              <View
                style={styles.item}
              >
                <MediaItem
                  data={data}
                />
              </View>
            </TouchableWithoutFeedback>
          ))}
        </View>
        {listPopuler.length > 0 && (
          <Button
            light
            block
            style={styles.button}
            onPress={() => navigation.navigate('ListProduct', {
              populer: true
            })}
          >
            <Text style={[globalStyle.h4]}>Lihat yang lain</Text>
          </Button>
        )}
      </View>
    )
  }
}

Populer.propTypes = {
  navigation: PropTypes.object.isRequired,
  listPopuler: PropTypes.array
}

Populer.defaultProps = {
  listPopuler: []
}

const mapStateToProps = state => ({
  listPopuler: state.home.listPopuler
})

export default connect(mapStateToProps)(Populer)
