import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  StyleSheet
} from 'react-native'
import {
  View,
  Text
} from 'native-base'
import SegmentItem from 'components/SegmentItem'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  content: {
    flex: 1,
    padding: 10
  },
  container: {
    width: '100%',
    flexDirection: 'row',
    flexWrap: 'wrap'
  }
})

class Daerah extends PureComponent {
  render () {
    const { listDaerah, navigation } = this.props
    return (
      <View style={styles.content}>
        {listDaerah.length > 0 && <Text style={[globalStyle.h2, { paddingVertical: 5 }]}>Daerah</Text>}
        {listDaerah.length > 0 && (
          <View style={styles.container}>
            <SegmentItem type="daerah" data={listDaerah} navigation={navigation} />
          </View>
        )}
      </View>
    )
  }
}

Daerah.propTypes = {
  listDaerah: PropTypes.array
}

Daerah.defaultProps = {
  listDaerah: []
}

const mapStateToProps = state => ({
  listDaerah: state.home.listDaerah
})

export default connect(mapStateToProps)(Daerah)
