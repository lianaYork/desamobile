import React, { PureComponent } from 'react'
import { connect } from 'react-redux'
import {
  StyleSheet
} from 'react-native'
import { RNCamera } from 'react-native-camera'
import {
  Container,
  Footer,
  Button,
  Text
} from 'native-base'
import color from 'theme/color'
import { update } from 'services/orderService'
import { getData } from 'actions/orderAction'

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  footer: {
    backgroundColor: color.textIcons,
    padding: 5,
    justifyContent: 'center',
    alignItems: 'center'
  }
})

class Payment extends PureComponent {
  takePicture = async () => {
    const { navigation, fetchOrder } = this.props
    const { order } = navigation.state.params
    try {
      const updateData = await update({ paidStatus: true }, order)
      if (updateData.success) {
        fetchOrder()
        navigation.navigate('Order')
      }
    } catch (error) {
      console.log('error', error)
    }
  }

  render () {
    return (
      <Container>
        <RNCamera
          ref={(ref) => { this.camera = ref }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.off}
          permissionDialogTitle="Permission to use camera"
          permissionDialogMessage="We need your permission to use your camera phone"
          onGoogleVisionBarcodesDetected={({ barcodes }) => {
            console.log(barcodes)
          }}
        />
        <Footer style={styles.footer}>
          <Button primary style={styles.btn} onPress={() => this.takePicture()}>
            <Text style={styles.submitText}>Capture</Text>
          </Button>
        </Footer>
      </Container>
    )
  }
}

const mapStateToProps = () => ({})

const mapDispatchToProps = dispatch => ({
  fetchOrder: () => dispatch(getData())
})

export default connect(mapStateToProps, mapDispatchToProps)(Payment)
