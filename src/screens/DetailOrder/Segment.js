import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  View
} from 'native-base'
import ImageSlider from 'components/ImageSlider'
import { IMAGEURL } from 'utils/config'

const styles = StyleSheet.create({
  content: {
    flex: 1
  },
  segmentImage: {
    height: 150
  }
})

class Segment extends PureComponent {
  render () {
    const { product } = this.props
    return (
      <View style={styles.content}>
        <ImageSlider
          style={styles.stretch}
          arrow={false}
          height={200}
          downloadable={false}
          dataSource={product.image.map(data => ({ ...data, url: `${IMAGEURL}${data.url}` }))}
        />
      </View>
    )
  }
}

export default Segment
