import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  Body,
  List,
  ListItem,
  Text,
  Left,
  Right,
  Button,
  Card,
  CardItem,
  View
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'

const styles = StyleSheet.create({
  cardItem: {
    paddingLeft: 10,
    alignItems: 'flex-start',
    marginLeft: 0,
    borderTopWidth: 1
  },
  button: {
    height: 50,
    width: 50,
    justifyContent: 'center',
    borderRadius: 25,
    backgroundColor: color.primaryColor
  },
  textPlan: {
    marginTop: 5,
    marginBottom: 10,
    fontWeight: 'normal'
  }
})

class ListDescription extends PureComponent {
  render () {
    const { product } = this.props
    return (
      <List>
        <ListItem style={styles.cardItem}>
          <Left>
            <Text style={[globalStyle.h4, { color: color.primaryText }]}>Participant</Text>
          </Left>
          <Right>
            <Text style={[globalStyle.h2, { fontWeight: 'normal' }]}>{`${product.participants.length || 0} orang`}</Text>
          </Right>
        </ListItem>
        <ListItem style={styles.cardItem}>
          <Left>
            <Text style={[globalStyle.h4, { color: color.primaryText }]}>Waktu</Text>
          </Left>
          <Right>
            <Text style={[globalStyle.h2, { fontWeight: 'normal' }]}>{`${product.duration} ${product.durationOption}`}</Text>
          </Right>
        </ListItem>
        <View
          style={{
            borderTopWidth: 1,
            borderColor: color.lightColor,
            paddingHorizontal: 10,
            paddingTop: 17
          }}
        >
          <Text style={[globalStyle.h4, { marginBottom: 10, color: color.primaryText }]}>Plan</Text>
          {
            product.plans.map((plan, index) => {
              return (
                <Card transparent key={index}>
                  <CardItem
                    style={[
                      styles.cardItem,
                      {
                        borderBottomWidth: 0,
                        borderTopWidth: 0,
                        paddingLeft: 0,
                        paddingTop: 0,
                        paddingBottom: 0,
                        backgroundColor: 'transparent'
                      }]}
                  >
                    <Left>
                      <Button style={styles.button} rounded disabled>
                        <Text>{`H${plan.day}`}</Text>
                      </Button>
                      <Body>
                        {plan.list.split(',').map((name, index) => (
                          <Text key={index} style={[globalStyle.h2, styles.textPlan]}>{name}</Text>
                        ))}
                      </Body>
                    </Left>
                  </CardItem>
                </Card>
              )
            })
          }
        </View>
        <ListItem style={styles.cardItem}>
          <Text style={[globalStyle.h2, { fontWeight: 'normal' }]}>Transfer ke Rek 21093102910 (BCA)</Text>
        </ListItem>
      </List>
    )
  }
}

export default ListDescription
