import React, { PureComponent } from 'react'
import {
  TouchableOpacity,
  StatusBar,
  ScrollView,
  RefreshControl
} from 'react-native'
import {
  View,
  Container,
  Icon,
  Text,
  Button
} from 'native-base'
import color from 'theme/color'
import globalStyle from 'theme/style'
import { getById } from 'services/orderService'
import Header from './Header'
import Segment from './Segment'
import ProductTitle from './ProductTitle'
import ListDescription from './ListDescription'

class DetailOrder extends PureComponent {
  static navigationOptions = ({ navigation }) => {
    let headerTitle = 'Detail Order'
    let headerTitleStyle = {
      marginHorizontal: 0,
      color: color.textIcons,
      fontSize: 18,
      flex: 1
    }
    let headerTintColor = color.textIcons
    let headerStyle = {
      height: 60,
      backgroundColor: color.primaryColor,
      backgroundOpacity: 0.7
    }
    let headerBackTitle = ''
    return {
      headerTitle,
      headerTitleStyle,
      headerStyle,
      headerTintColor,
      headerBackTitle,
      backBehavior: 'initialRoute',
      headerLeft: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
          onPress={() => { navigation.goBack(null) }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="ios-arrow-back"
            />
          </View>
        </TouchableOpacity>
      ),
      headerRight: (
        <TouchableOpacity
          style={{
            paddingHorizontal: 10
          }}
        >
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center'
            }}
          >
            <Icon
              style={{
                color: color.textIcons
              }}
              name="share-google"
              type="EvilIcons"
            />
          </View>
        </TouchableOpacity>
      )
    }
  }

  state = {
    product: {},
    plans: [],
    refreshing: false
  }

  componentWillMount () {
    this.getProduct()
  }

  getProduct () {
    const { navigation } = this.props
    if (navigation.state.params) {
      const { product, plans } = navigation.state.params
      this.setProduct(product, plans)
    }
  }

  setProduct (product, plans) {
    this.setState({ product, plans })
  }

  _refresh = async () => {
    const { product } = this.state
    try {
      const productData = await getById(product.order_id)
      if (productData.success) {
        this.setState({
          product: {
            ...productData.data.product,
            paidStatus: productData.data.paidStatus,
            participants: productData.data.participants,
            order_id: product.order_id,
            city: product.city,
            province: product.province
          }
        })
      }
    } catch (error) {
      console.log('error', error)
    }
  }

  render () {
    const { navigation } = this.props
    const { product, plans, refreshing } = this.state
    return (
      <Container>
        <StatusBar backgroundColor={color.primaryColor} />
        <ScrollView
          scrollEventThrottle={300}
          removeClippedSubviews
          refreshControl={(
            <RefreshControl
              refreshing={refreshing}
              onRefresh={() => this._refresh()}
            />
          )}
        >
          <Header navigation={navigation} />
          <Segment product={product} />
          <ProductTitle product={product} />
          <ListDescription product={{ ...product, plans }} />
          <View style={{ padding: 10 }}>
            <Button
              onPress={() => navigation.navigate('Participant', {
                order: product.order_id,
                paidStatus: product.paidStatus
              })}
              style={[globalStyle.buttonCenter, { backgroundColor: color.primaryColor }]}
            >
              <Text>Partisipan</Text>
            </Button>
          </View>
          {!product.paidStatus && (
            <View style={{ paddingHorizontal: 10, paddingBottom: 10 }}>
              <Button
                bordered
                onPress={() => navigation.navigate('Payment', {
                  order: product.order_id
                })}
                style={[globalStyle.buttonCenter]}
              >
                <Text>Konfirmasi Pembayaran</Text>
              </Button>
            </View>
          )}
        </ScrollView>
      </Container>
    )
  }
}

export default DetailOrder
