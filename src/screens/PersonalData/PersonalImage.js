import React, { PureComponent } from 'react'
import {
  StyleSheet
} from 'react-native'
import {
  Item,
  Left,
  View,
  Text
} from 'native-base'
import FastImage from 'react-native-fast-image'
import color from 'theme/color'
import { IMAGEURL } from 'utils/config'

const styles = StyleSheet.create({
  image: {
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  item: {
    borderBottomWidth: 0
  },
  icon: {
    borderRadius: 20,
    height: 120,
    flex: 1
  }
})

class PersonalImage extends PureComponent {
  render () {
    const { data } = this.props
    return (
      <View style={styles.image}>
        <Item style={styles.item}>
          <FastImage
            style={styles.icon}
            source={{
              uri: `${IMAGEURL}${data.image.url}`
            }}
            resizeMode={FastImage.resizeMode.contain}
          />
          <Left>
            <Text style={{ color: color.primaryColor, marginVertical: 5 }}>Ganti Foto</Text>
            <Text style={{ color: color.primaryText, marginVertical: 5 }}>Ganti Password</Text>
          </Left>
        </Item>
      </View>
    )
  }
}

export default PersonalImage
