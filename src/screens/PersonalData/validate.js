const validate = ({
  firstName,
  lastName,
  birthDate,
  gender,
  idNo,
  address
}) => {
  const errors = {
    firstName: '',
    lastName: '',
    birthDate: '',
    gender: '',
    idNo: '',
    address: ''
  }

  errors.firstName = !firstName ? '*' : undefined
  errors.lastName = !lastName ? '*' : undefined
  errors.birthDate = !birthDate ? '*' : undefined
  errors.gender = !gender ? '*' : undefined
  errors.idNo = !idNo ? '*' : undefined
  errors.address = !address ? '*' : undefined

  return errors
}

export default validate
