import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  Alert,
  Platform,
  BackHandler,
  KeyboardAvoidingView,
  ScrollView
} from 'react-native'
import Header from 'components/Header'

import PersonalImage from './PersonalImage'
import PersonalForm from './PersonalForm'

class PersonalData extends PureComponent {
  componentDidMount () {
    if (Platform.OS === 'android') {
      BackHandler.addEventListener('hardwareBackPress', this.goBack)
    }
  }

  componentWillUnmount () {
    BackHandler.removeEventListener('hardwareBackPress', this.goBack)
  }

  goBack = () => {
    const { navigation } = this.props
    Alert.alert(
      'Kembali ?',
      '',
      [
        {
          text: 'Batalkan',
          onPress: () => { },
          style: 'cancel'
        },
        { text: 'OK', onPress: () => navigation.goBack(null) }
      ],
      { cancelable: false },
    )
    return true
  }

  render () {
    const { personalInfo } = this.props

    return (
      <ScrollView>
        <KeyboardAvoidingView
          behavior="padding"
          enabled
        >
          <Header goBack={this.goBack} name="Profile" />
          <PersonalImage data={personalInfo} />
          <PersonalForm data={personalInfo} />
        </KeyboardAvoidingView>
      </ScrollView>
    )
  }
}

PersonalData.propTypes = {
  personalInfo: PropTypes.object
}

PersonalData.defaultProps = {
  personalInfo: {}
}

const mapStateToProps = state => ({
  personalInfo: state.personal.personalInfo
})

export default connect(mapStateToProps)(PersonalData)
