import React, { PureComponent } from 'react'
import { Field, reduxForm } from 'redux-form'
import {
  StyleSheet
} from 'react-native'
import {
  Input,
  Item,
  View,
  Text,
  Button
} from 'native-base'
import color from 'theme/color'
import validate from './validate'

const styles = StyleSheet.create({
  image: {
    padding: 10
  },
  label: {
    color: color.primaryText,
    fontSize: 14,
    marginBottom: 5
  },
  viewInput: {
    marginBottom: 10
  },
  input: {
    paddingLeft: 10,
    paddingRight: 10,
    color: color.primaryText,
    borderWidth: 1,
    fontSize: 14,
    borderColor: color.dividerColor,
    borderRadius: 5,
    height: 40
  },
  item: {
    borderBottomWidth: 0
  }
})

class PersonalForm extends PureComponent {
  onSubmit = () => {
    console.log('submit')
  }

  changeValue = async (text, key) => {
    const { change } = this.props
    await change(key, text)
  }

  render () {
    const {
      handleSubmit,
      submitting,
      invalid,
      error
    } = this.props

    return (
      <View style={styles.image}>
        <View style={styles.viewInput}>
          <Text style={styles.label}>Nama Awal</Text>
          <Field
            name="firstName"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'firstName')}
                  placeholder="e.g. John"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <View style={styles.viewInput}>
          <Text style={styles.label}>Nama Akhir</Text>
          <Field
            name="lastName"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'lastName')}
                  placeholder="e.g. Doe"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <View style={styles.viewInput}>
          <Text style={styles.label}>Tanggal Lahir</Text>
          <Field
            name="birthDate"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'birthDate')}
                  placeholder="DD/MMM/YYYY"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <View style={styles.viewInput}>
          <Text style={styles.label}>Jenis Kelamin</Text>
          <Field
            name="gender"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'gender')}
                  placeholder="Male"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <View style={styles.viewInput}>
          <Text style={styles.label}>No Identitas</Text>
          <Field
            name="idNo"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'idNo')}
                  placeholder="e.g. 112181293812939"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <View style={styles.viewInput}>
          <Text style={styles.label}>Alamat</Text>
          <Field
            name="address"
            component={({ input, meta: { error, touched } }) => (
              <Item
                style={styles.item}
                error={!!error && touched}
              >
                <Input
                  onChangeText={text => this.changeValue(text, 'address')}
                  placeholder="e.g. Jalan Perintis Kemerdekaan No 10"
                  style={styles.input}
                  value={input.value}
                />
                {touched && error && <Text style={{ color: color.errorColor }}>{error}</Text>}
              </Item>
            )}
          />
        </View>
        <Button
          primary
          block
          disabled={invalid || submitting}
          style={styles.btnNext}
          onPress={handleSubmit(this.onSubmit)}
        >
          <Text
            style={{ alignSelf: 'center', color: color.textIcons }}
          >
            Ubah
          </Text>
        </Button>
        {error && <Text>{error}</Text>}
      </View>
    )
  }
}

export default reduxForm({
  form: 'PersonalForm',
  validate
})(PersonalForm)
