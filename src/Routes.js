import React from 'react'
import {
  Image,
  StyleSheet,
  TouchableWithoutFeedback
} from 'react-native'
import {
  Container,
  Text,
  View,
  Icon
} from 'native-base'
import {
  createStackNavigator,
  createBottomTabNavigator,
  createSwitchNavigator
} from 'react-navigation'
import TabBarItem from 'components/TabBarItem'
import Home from 'screens/Home'
import DetailProduct from 'screens/DetailProduct'
import DetailOrder from 'screens/DetailOrder'
import Payment from 'screens/Payment'
import Participant from 'screens/Participant'
import DetailParticipant from 'screens/DetailParticipant'
import ListProduct from 'screens/ListProduct'
import Search from 'screens/Search'
import Order from 'screens/Order'
import Favourite from 'screens/Favourite'
import Profile from 'screens/Profile'
import PersonalData from 'screens/PersonalData'
import History from 'screens/History'
import Privacy from 'screens/Privacy'
import Term from 'screens/Term'
import Void from 'screens/Void'
import Report from 'screens/Report'
import SignIn from 'screens/SignIn'
import SignUp from 'screens/SignUp'
import AuthLoadingScreen from 'screens/AuthLoadingScreen'
import color from 'theme/color'
import { APPNAME } from 'utils/config'
import { headerOption } from 'utils/components'

const styles = StyleSheet.create({
  header: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    paddingBottom: 2
  },
  headerText: {
    fontSize: 20,
    color: color.textIcons
  }
})

const BottomTabNavigatorConfig = {
  initialRouteName: 'Home',
  backBehavior: 'none',
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  mode: 'card',
  tabBarOptions: {
    showIcon: true,
    activeTintColor: color.primaryColor,
    inactiveTintColor: color.secondaryText,
    style: {
      borderTopWidth: 1,
      elevation: 6,
      backgroundColor: color.textIcons,
      height: 60
    },
    labelStyle: {
      marginTop: -4,
      fontSize: 12
    },
    indicatorStyle: {
      height: 0
    }
  }
}

const HomeScreen = createStackNavigator({
  HomeScreen: {
    screen: Home,
    navigationOptions: headerOption({
      headerTitle: (
        <View style={styles.header}>
          <Image
            style={{
              width: 36,
              height: 36
            }}
            resizeMode="contain"
            source={require('../assets/app/logo.png')}
          />
          <Text style={styles.headerText}>{APPNAME}</Text>
        </View>
      )
    })
  }
})

const Main = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => {
      return ({
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused, tintColor }) => (
          <TabBarItem
            tintColor={tintColor}
            focused={focused}
            iconName={focused ? 'home' : 'home-outline'}
            typeIcon={focused ? 'Foundation' : 'MaterialCommunityIcons'}
          />
        )
      })
    }
  },
  Order: {
    screen: createStackNavigator({
      OrderScreen: {
        screen: Order,
        navigationOptions: ({ navigation }) => headerOption({
          headerTitle: (
            <TouchableWithoutFeedback onPress={() => navigation.navigate('Search')}>
              <View style={styles.header}>
                <Icon
                  style={{
                    marginRight: 16,
                    fontSize: 20,
                    color: color.textIcons
                  }}
                  name="search"
                  type="FontAwesome"
                />
                <Text style={styles.headerText}>{`Cari di ${APPNAME}`}</Text>
              </View>
            </TouchableWithoutFeedback>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Pemesanan',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'address-book' : 'address-book-o'}
          typeIcon="FontAwesome"
        />
      )
    })
  },
  Favourite: {
    screen: createStackNavigator({
      FavouriteScreen: {
        screen: Favourite,
        navigationOptions: headerOption({
          headerTitle: (
            <View style={styles.header}>
              <Text style={styles.headerText}>Minat</Text>
            </View>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Minat',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'heart' : 'hearto'}
          typeIcon="AntDesign"
        />
      )
    })
  },
  Profile: {
    screen: createStackNavigator({
      FavouriteScreen: {
        screen: Profile,
        navigationOptions: headerOption({
          headerTitle: (
            <View style={styles.header}>
              <Text style={styles.headerText}>Informasi</Text>
            </View>
          )
        })
      }
    }),
    navigationOptions: () => ({
      tabBarLabel: 'Informasi',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'user' : 'user-o'}
          typeIcon="FontAwesome"
        />
      )
    })
  }
}, BottomTabNavigatorConfig)

const tabBarOnPress = ({ navigation }) => {
  navigation.navigate('SignIn')
}

const MainAuth = createBottomTabNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: () => {
      return ({
        tabBarLabel: 'Home',
        tabBarIcon: ({ focused, tintColor }) => (
          <TabBarItem
            tintColor={tintColor}
            focused={focused}
            iconName={focused ? 'home' : 'home-outline'}
            typeIcon={focused ? 'Foundation' : 'MaterialCommunityIcons'}
          />
        )
      })
    }
  },
  Order: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarOnPress,
      tabBarLabel: 'Pemesanan',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'address-book' : 'address-book-o'}
          typeIcon="FontAwesome"
        />
      )
    })
  },
  Favourite: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarOnPress,
      tabBarLabel: 'Minat',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'heart' : 'hearto'}
          typeIcon="AntDesign"
        />
      )
    })
  },
  Profile: {
    screen: SignIn,
    navigationOptions: () => ({
      tabBarOnPress,
      tabBarLabel: 'Profile',
      tabBarIcon: ({ focused, tintColor }) => (
        <TabBarItem
          focused={focused}
          tintColor={tintColor}
          iconName={focused ? 'user' : 'user-o'}
          typeIcon="FontAwesome"
        />
      )
    })
  }
}, BottomTabNavigatorConfig)

const LoginStack = createStackNavigator(
  {
    MainAuth: {
      screen: MainAuth
    },
    SignIn: {
      screen: SignIn
    },
    SignUp: {
      screen: createStackNavigator({
        SignUpScreen: {
          screen: SignUp,
          navigationOptions: headerOption({
            headerTitle: (
              <View style={styles.header}>
                <Image
                  style={{
                    width: 36,
                    height: 36
                  }}
                  resizeMode="contain"
                  source={require('../assets/app/logo.png')}
                />
                <Text style={styles.headerText}>Register</Text>
              </View>
            )
          })
        }
      })
    },
    Search: {
      screen: Search
    },
    ListProduct: {
      screen: ListProduct
    },
    DetailProduct: {
      screen: DetailProduct
    }
  }, {
    initialRouteName: 'MainAuth',
    navigationOptions: { header: null }
  }
)

const RequireAuth = createStackNavigator(
  {
    Main: {
      screen: Main
    },
    Search: {
      screen: Search
    },
    ListProduct: {
      screen: ListProduct
    },
    DetailProduct: {
      screen: DetailProduct
    },
    PersonalData: {
      screen: PersonalData
    },
    History: {
      screen: History
    },
    Privacy: {
      screen: Privacy
    },
    Term: {
      screen: Term
    },
    Void: {
      screen: Void
    },
    Report: {
      screen: Report
    },
    DetailOrder: {
      screen: DetailOrder
    },
    Payment: {
      screen: Payment
    },
    Participant: {
      screen: Participant
    },
    DetailParticipant: {
      screen: DetailParticipant
    }
  }, {
    initialRouteName: 'Main',
    navigationOptions: { header: null }
  }
)

const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen, // Utk check login
    App: RequireAuth, // Utk required login
    Auth: LoginStack // Utk not required
  }, {
    initialRouteName: 'AuthLoading'
  }
)

export default class Routes extends React.Component {
  render () {
    return (
      <Container>
        <AppNavigator />
      </Container>
    )
  }
}
