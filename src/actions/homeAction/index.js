import { Toast } from 'native-base'
import { get as getProduct } from 'services/productService'
import { get as getDaerah } from 'services/daerahService'
import { get as getHobby } from 'services/hobbyService'
import { get as getTerm } from 'services/termService'
import { FETCH_HOME, RECEIVE_HOME, FAILED_HOME } from 'actions/types'

const fetchHome = () => {
  return {
    type: FETCH_HOME
  }
}

const receiveHome = (object) => {
  return {
    type: RECEIVE_HOME,
    payload: {
      ...object
    }
  }
}

const failedHome = (error) => {
  Toast.show({
    text: error,
    buttonText: 'Okay',
    type: 'warning'
  })
  return {
    type: FAILED_HOME,
    payload: {
      error
    }
  }
}

const queryHome = () => async (dispatch) => {
  const query = {
    _sort: 'name',
    _limit: 4
  }

  try {
    const termData = await getTerm({ slug: 'main-page-segment' })
    dispatch(receiveHome({
      segmentImage: termData.data[0] || {}
    }))
    dispatch(fetchHome())
    const productData = await getProduct(query)
    const daerahData = await getDaerah(query)
    const hobbyData = await getHobby(query)

    if (termData.data[0]) {
      termData.data = termData.data[0]
    } else {
      termData.data = {}
    }

    dispatch(receiveHome({
      listPopuler: productData.data || [],
      listDaerah: daerahData.data || [],
      listHobby: hobbyData.data || []
    }))
  } catch (error) {
    const { message } = error
    return dispatch(failedHome(message))
  }
}

export {
  queryHome
}
