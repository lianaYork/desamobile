import {
  FETCH_TOKEN,
  FETCH_USER,
  AUTH_ERROR,
  SET_CURRENT_USER
} from 'actions/types'
import {
  service
} from 'actions/serviceAction'

function fetchToken () {
  return {
    type: FETCH_TOKEN
  }
}

function userLogin () {
  return {
    type: FETCH_USER
  }
}

function setCurrentUser (user) {
  return {
    type: SET_CURRENT_USER,
    user
  }
}

function authError (message) {
  return {
    type: AUTH_ERROR,
    errorMessage: message
  }
}

const signin = () => async (dispatch) => {
  dispatch(fetchToken())
}

const fetchUser = () => async (dispatch) => {
  dispatch(fetchToken())
}

const signup = () => async (dispatch) => {
  dispatch(service())
}

export {
  fetchToken,
  userLogin,
  fetchUser,
  setCurrentUser,
  authError,
  signin,
  signup
}
