// auth
export const SET_CURRENT_USER = 'SET_CURRENT_USER'
export const FETCH_TOKEN = 'FETCH_TOKEN'
export const AUTH_ERROR = 'AUTH_ERROR'
export const FETCH_USER_SUCCESS = 'FETCH_USER_SUCCESS'

// user
export const FETCH_USER = 'FETCH_USER'
export const RECEIVE_USER = 'RECEIVE_USER'
export const UPDATE_USER = 'UPDATE_USER'
export const UPDATE_USER_SUCCESS = 'UPDATE_USER_SUCCESS'
export const UPDATE_USER_FAILURE = 'UPDATE_USER_FAILURE'

// service
export const SERVICE = 'SERVICE'
export const SERVICE_SUCCESS = 'SERVICE_SUCCESS'
export const SERVICE_FAILURE = 'SERVICE_FAILURE'
export const POSTING = 'POSTING'
export const POST_SUCCESS = 'POST_SUCCESS'
export const POST_FAILURE = 'POST_FAILURE'

// home
export const FETCH_HOME = 'FETCH_HOME'
export const RECEIVE_HOME = 'RECEIVE_HOME'
export const FAILED_HOME = 'FAILED_HOME'

// product
export const FETCH_PRODUCT = 'FETCH_PRODUCT'
export const RECEIVE_PRODUCT = 'RECEIVE_PRODUCT'

// order
export const FETCH_ORDER = 'FETCH_ORDER'
export const RECEIVE_ORDER = 'RECEIVE_ORDER'
export const FAILED_ORDER = 'FAILED_ORDER'
export const CHANGE_ORDER_DATE = 'CHANGE_ORDER_DATE'
