import {
  FETCH_ORDER,
  RECEIVE_ORDER,
  FAILED_ORDER,
  CHANGE_ORDER_DATE
} from 'actions/types'
import { me } from 'services/userService'
import {
  get,
  set
} from 'services/orderService'
import { Toast } from 'native-base'
import moment from 'moment'

const fetchData = () => {
  return {
    type: FETCH_ORDER
  }
}

const receiveData = ({ listOrder }) => {
  return {
    type: RECEIVE_ORDER,
    payload: {
      listOrder
    }
  }
}

const failedData = ({ error }) => {
  Toast.show({
    text: error,
    buttonText: 'Okay',
    type: 'warning'
  })
  return {
    type: FAILED_ORDER,
    payload: {
      errorMessage: error
    }
  }
}

const changeVarOrderData = (orderDate) => {
  return {
    type: CHANGE_ORDER_DATE,
    payload: {
      orderDate
    }
  }
}

const resetDate = () => async (dispatch) => {
  dispatch(changeVarOrderData(null))
}

const getData = () => async (dispatch) => {
  dispatch(fetchData())

  try {
    const user = await me()
    if (user.success) {
      const query = { _sort: 'date', done: false, user: user.data.id }
      const data = await get(query)

      if (data.success) {
        dispatch(receiveData({
          listOrder: data.data
        }))
      } else {
        dispatch(failedData(data.message))
      }
    } else {
      dispatch(failedData(user.message))
    }
  } catch (error) {
    dispatch(failedData(error.message))
  }
}

const changeDate = ({
  date,
  product,
  navigation
}) => async (dispatch) => {
  const user = await me()
  if (user.success) {
    const currentOrder = await get({
      done: false,
      paidStatus: false,
      active: true
    })
    if (currentOrder.success) {
      if (currentOrder.data.length === 0) {
        const dataOrder = {
          product: product.id,
          price: product.price,
          priceDiscount: product.priceDiscount,
          orderStatus: 'Menunggu',
          active: true,
          paidStatus: false,
          done: false,
          plans: product.plans.map(data => data.id),
          user: user.data.id,
          date: moment(date).format('YYYY-MM-DD'),
          city: product.city.id,
          province: product.province.id
        }
        try {
          const orderInsert = await set(dataOrder)

          if (orderInsert.success) {
            Toast.show({
              text: 'Lengkapi Partisipan',
              buttonText: 'Okay',
              type: 'success',
              duration: 5000,
              onClose: () => navigation.navigate('DetailOrder', {
                plans: product.plans,
                product: {
                  ...product,
                  paidStatus: false,
                  participants: [],
                  order_id: orderInsert.data.id
                }
              })
            })
          } else {
            Toast.show({
              text: orderInsert.message,
              buttonText: 'Okay',
              type: 'warning'
            })
          }
        } catch (error) {
          Toast.show({
            text: error.message,
            buttonText: 'Okay',
            type: 'warning'
          })
        }
        dispatch(changeVarOrderData(date))
      } else {
        Toast.show({
          text: 'Selesaikan pembayaran sebelumnya',
          buttonText: 'Okay',
          type: 'warning'
        })
      }
    }
  } else {
    navigation.navigate('SignIn')
  }
}

export {
  getData,
  changeDate,
  resetDate
}
