import {
  FETCH_HOME,
  RECEIVE_HOME
} from '../actions/types'

const initialState = {
  loadingFavourite: false,
  errorMessage: '',
  listFavourite: [
    // {
    //   id: 1,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 0,
    //   minParticipant: 2,
    //   duration: '2 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 2,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 1,
    //   minParticipant: 2,
    //   duration: '2 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 3,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: null,
    //   priceDiscountText: null,
    //   status: 1,
    //   minParticipant: 2,
    //   duration: '2 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 4,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 1,
    //   minParticipant: 2,
    //   duration: '2 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // }
  ]
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_HOME: {
      return Object.assign({}, state, {
        loadingHome: true
      })
    }
    case RECEIVE_HOME: {
      return Object.assign({}, state, {
        loadingHome: false
      })
    }
    default:
      return state
  }
}
