import {
  FETCH_ORDER,
  RECEIVE_ORDER,
  FAILED_HOME,
  CHANGE_ORDER_DATE
} from '../actions/types'

const initialState = {
  loadingOrder: false,
  errorMessage: '',
  orderDate: null,
  listOrder: [
    // {
    //   id: 1,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 0,
    //   orderStatus: 'Diteruskan ke pemilik',
    //   duration: '2 hari',
    //   participant: 12,
    //   paidStatus: 1,
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 2,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 1,
    //   orderStatus: 'Menunggu',
    //   duration: '2 Jam',
    //   participant: 12,
    //   availableParticipant: 2,
    //   paidStatus: 1,
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 3,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: null,
    //   priceDiscountText: null,
    //   status: 1,
    //   orderStatus: 'Diteruskan ke pemilik',
    //   duration: '2 hari',
    //   participant: 12,
    //   paidStatus: 0,
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 4,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   status: 1,
    //   orderStatus: 'Diteruskan ke pemilik',
    //   duration: '2 Jam',
    //   participant: 12,
    //   paidStatus: 1,
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // }
  ]
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_ORDER: {
      return Object.assign({}, state, {
        loadingOrder: true
      })
    }
    case RECEIVE_ORDER: {
      return Object.assign({}, state, {
        loadingOrder: false,
        listOrder: action.payload.listOrder
      })
    }
    case FAILED_HOME: {
      return Object.assign({}, state, {
        loadingOrder: false
      })
    }
    case CHANGE_ORDER_DATE: {
      return Object.assign({}, state, {
        orderDate: action.payload.orderDate
      })
    }
    default:
      return state
  }
}
