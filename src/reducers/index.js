import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import auth from './auth'
import home from './home'
import favourite from './favourite'
import search from './search'
import order from './order'
import personal from './personal'
import product from './product'

export default combineReducers({
  form: formReducer,
  auth,
  home,
  favourite,
  search,
  order,
  personal,
  product
})
