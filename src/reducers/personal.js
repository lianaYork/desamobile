import {
  FETCH_HOME,
  RECEIVE_HOME
} from '../actions/types'

const initialState = {
  loadingPersonal: false,
  errorMessage: '',
  personalInfo: {
    id: 12,
    image: {
      url: '/image7.jpg'
    },
    firstName: 'John',
    lastName: 'Doe',
    birthDate: '1995-01-01',
    gender: 1,
    idNo: '112190213901231',
    address: 'Jalan Perintis Kemerdekaan No 10'
  }
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_HOME: {
      return Object.assign({}, state, {
        loadingPersonal: true
      })
    }
    case RECEIVE_HOME: {
      return Object.assign({}, state, {
        loadingPersonal: false
      })
    }
    default:
      return state
  }
}
