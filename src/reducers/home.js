import {
  FETCH_HOME,
  RECEIVE_HOME,
  FAILED_HOME
} from '../actions/types'

const initialState = {
  loadingHome: false,
  errorMessage: '',
  segmentImage: {},
  listPopuler: [
    // {
    //   id: 1,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   minParticipant: 1,
    //   duration: '2 Hari',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 2,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   minParticipant: 2,
    //   duration: '3 Hari',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 3,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   minParticipant: 2,
    //   duration: '4 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // },
    // {
    //   id: 4,
    //   image: {
    //     url: '/image1.jpg'
    //   },
    //   name: 'Nama Wisata',
    //   province: 'Provinsi',
    //   city: 'Kota',
    //   price: 200000,
    //   priceText: 'Rp 200.000',
    //   priceDiscount: 350000,
    //   priceDiscountText: 'Rp 350.000',
    //   minParticipant: 2,
    //   duration: '2 Jam',
    //   plan: [
    //     {
    //       id: 1,
    //       day: 1,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     },
    //     {
    //       id: 2,
    //       day: 2,
    //       list: ['Diving melihat terumbu', 'Memancing', 'BBQ dengan ikan laut']
    //     }
    //   ]
    // }
  ],
  listHobby: [
    // {
    //   id: '1',
    //   image: {
    //     url: '/image3.jpg'
    //   },
    //   name: 'Hiking',
    //   prefix: 'adalah',
    //   description: 'Lorem ipsum dolor sit amet, admodum omnesque et sed, laoreet eloquentiam consequuntur eu per. Pri putent vivendum omittantur et, tation epicurei ne duo. Id libris perpetua aliquando est. Pro mucius oblique at. Ei utroque lobortis inciderint eum, an pri cibo copiosae petentium, persius repudiandae sea ex.',
    //   planCount: 40
    // },
    // {
    //   id: '2',
    //   image: {
    //     url: '/image2.jpg'
    //   },
    //   name: 'Fishing',
    //   prefix: 'adalah',
    //   description: 'Lorem ipsum dolor sit amet, admodum omnesque et sed, laoreet eloquentiam consequuntur eu per. Pri putent vivendum omittantur et, tation epicurei ne duo. Id libris perpetua aliquando est. Pro mucius oblique at. Ei utroque lobortis inciderint eum, an pri cibo copiosae petentium, persius repudiandae sea ex.',
    //   planCount: 40
    // }
  ],
  listDaerah: [
    // {
    //   id: '1',
    //   image: {
    //     url: '/image4.jpg'
    //   },
    //   name: 'Sumatera Utara',
    //   description: 'Lorem ipsum dolor sit amet, admodum omnesque et sed, laoreet eloquentiam consequuntur eu per. Pri putent vivendum omittantur et, tation epicurei ne duo. Id libris perpetua aliquando est. Pro mucius oblique at. Ei utroque lobortis inciderint eum, an pri cibo copiosae petentium, persius repudiandae sea ex.',
    //   planCount: 40
    // },
    // {
    //   id: '2',
    //   image: {
    //     url: '/image5.jpg'
    //   },
    //   name: 'Sulawesi Selatan',
    //   description: 'Lorem ipsum dolor sit amet, admodum omnesque et sed, laoreet eloquentiam consequuntur eu per. Pri putent vivendum omittantur et, tation epicurei ne duo. Id libris perpetua aliquando est. Pro mucius oblique at. Ei utroque lobortis inciderint eum, an pri cibo copiosae petentium, persius repudiandae sea ex.',
    //   planCount: 40
    // }
  ]
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_HOME: {
      return Object.assign({}, state, {
        loadingHome: true
      })
    }
    case RECEIVE_HOME: {
      return Object.assign({}, state, {
        loadingHome: false,
        ...action.payload
      })
    }
    case FAILED_HOME: {
      return Object.assign({}, state, {
        loadingHome: false,
        errorMessage: action.payload.error
      })
    }
    default:
      return state
  }
}
